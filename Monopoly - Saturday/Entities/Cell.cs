﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monopoly___Saturday.Entities
{
    class Cell
    {
        string name;
        int price;
        int rent;
        PlayerTypes owner;
        int index;
        int x, y;

        public int Y
        {
            get
            {
                return this.y;
            }
            set
            {
                this.y = value;
            }
        }

        public int X
        {
            get
            {
                return this.x;
            }
            set
            {
                this.x = value;
            }
        }

        public int Index
        {
            get
            {
                return this.index;
            }
            set
            {
                this.index = value;
            }
        }

        public PlayerTypes Owner
        {
            get
            {
                return this.owner;
            }
            set
            {
                this.owner = value;
            }
        }

        public int Rent
        {
            get
            {
                return this.rent;
            }
            set
            {
                this.rent = value;
            }
        }

        public int Price
        {
            get
            {
                return this.price;
            }
            set
            {
                this.price = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }
    }
}

﻿using Monopoly___Saturday.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monopoly___Saturday.Forms
{
    class MonopolyGame
    {
        List<Player> playerList = new List<Player>();
        List<Cell> board = new List<Cell>();
        Dice dice;

        public List<Player> PlayerList
        {
            get
            {
                return this.playerList;
            }
            set
            {
                this.playerList = value;
            }
        }
    }
}

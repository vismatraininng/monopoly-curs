﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopoly___Saturday.Entities
{
    public class Player
    {
        string name;
        int money;
        PlayerTypes type; // 1=car, 2=shoe, 3=plane,...
        int position;

        public int Position
        {
            get
            {
                return this.position;
            }
            set
            {
                this.position = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public PlayerTypes Type
        {
            get
            {
                return this.type;
            }
            set
            {
                this.type = value;
            }
        }

        public int Money
        {
            get
            {
                return this.money;
            }
            set
            {
                this.money = value;
            }
        }

    }
}

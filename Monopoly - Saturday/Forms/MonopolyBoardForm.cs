﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopoly___Saturday.Forms
{
    public partial class MonopolyBoardForm : Form
    {
        MonopolyGame game = new MonopolyGame();

          public MonopolyBoardForm()
        {
            InitializeComponent();
        }

        public MonopolyBoardForm(List<Entities.Player> playerList)
        {
            game.PlayerList = playerList;
            InitializeComponent();
        }

        private void MonopolyBoardForm_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = game.PlayerList;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CellUserControl testCell = new CellUserControl();
            testCell.Parent = this;
            testCell.Visible = true;
            testCell.panel1.BackColor
                 = Color.Pink;
        }
    }
}

﻿using Monopoly___Saturday.Entities;
using Monopoly___Saturday.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopoly___Saturday
{
    public partial class NewGameForm : Form
    {

        List<Player> playerList = new List<Player>();

        public NewGameForm()
        {
            InitializeComponent();
        }

        private void startGameButton_Click(object sender, EventArgs e)
        {
            playerList.RemoveAll(player => player.Name == string.Empty);
            MonopolyBoardForm monopolyBoardForm = new MonopolyBoardForm(playerList);
            monopolyBoardForm.ShowDialog();
        }



        private void InitializePlayerList()
        {
            playerList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.Car });
            playerList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.Hat });
            playerList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.Shoe });
            playerList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.Tank });
            playerList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.Wheelbarrow });
        }

        private void NewGameForm_Load(object sender, EventArgs e)
        {
            InitializePlayerList();
            playersGridView.DataSource = playerList;

        }
    }
}
